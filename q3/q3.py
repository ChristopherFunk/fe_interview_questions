import re
import unittest

WORDS = 1
NUMBERS = 2


def logCmp(a, b):
    aType, aTokens = getErrorType(a)
    bType, bTokens = getErrorType(b)

    if aType > bType:
        # Comparing words to numbers
        return 1
    elif aType == WORDS and aType == bType:
        aStr = aTokens[1]
        bStr = bTokens[1]

        # If the strings are equal, use the ID's to sort
        if (aStr == bStr):
            aID = aTokens[0]
            bID = bTokens[0]
            return cmp(aID, bID)

        return cmp(aStr, bStr)
    elif aType == NUMBERS and bType == NUMBERS:
        return 0
    else:
        return -1


def getErrorType(error):
    tokens = error.split(" ", 1)

    if re.search("^[ 0-9 ]+$", tokens[1]) is not None:
        return NUMBERS, tokens
    else:
        return WORDS, tokens


class BracketsValidationTests(unittest.TestCase):

    def test_valid_brackets(self):
        logLines = [
            "b12 8 3 5 2",
            "v1 err var",
            "ap9 3 9",
            "hf2 err var",
            "t12 goog  ana",
            "u2 fa handle err"
        ]

        expectedOutput = [
            "hf2 err var",
            "v1 err var",
            "u2 fa handle err",
            "t12 goog  ana",
            "b12 8 3 5 2",
            "ap9 3 9"
        ]

        logLines.sort(logCmp)
        self.assertEqual(logLines, expectedOutput)

    def test_valid_brackets_with_upper_case(self):
        logLines = [
            "b12 8 3 5 2",
            "v1 err var",
            "ap9 3 9",
            "hf3 err var",
            "hf2 Err Var",
            "t12 goog  ana",
            "u2 fa handle err",
            "ap9 5 12 2",
        ]

        expectedOutput = [
            "hf2 Err Var",
            "hf3 err var",
            "v1 err var",
            "u2 fa handle err",
            "t12 goog  ana",
            "b12 8 3 5 2",
            "ap9 3 9",
            "ap9 5 12 2",
        ]

        logLines.sort(logCmp)
        self.assertEqual(logLines, expectedOutput)


if __name__ == '__main__':
    unittest.main()
