import sys

class Stack:
    def __init__(self):
        self.stack = []

    def push(self, bracket):
        self.stack.append(bracket)

    def pop(self):
        self.stack.pop()

    def top(self):
        if len(self.stack) == 0:
            return None

        return self.stack[-1]

    def isEmpty(self):
        return len(self.stack) == 0

    def __str__(self):
        return str(self.stack)


openBrackets = ["[", "(", "{",]
closeBrackets = ["]", ")", "}"]
matchBrackets = {
    "[": "]",
    "{": "}",
    "(": ")",
    "]": "[",
    "}": "{",
    ")": "(",
}


def validateBrackets(brackets=[]):
    if brackets is None:
        return False

    stack = Stack()

    for char in brackets:
        if char in openBrackets:
            stack.push(char)
        elif char in closeBrackets:
            match = stack.top()

            if match is not None and char == matchBrackets[match]:
                stack.pop()
            else:
                # Handles case where we have a close bracket before open
                return False

    if stack.isEmpty():
        return True
    else:
        return False


if __name__ == '__main__':
    if sys.argv[1] is None or not isinstance(sys.argv, list):
        print("Must pass in an array of brackets")

    isValid = validateBrackets(sys.argv[1])
     
    if isValid:
        print("Brackets are Valid")
    else:
        print("Brackets are Invalid")