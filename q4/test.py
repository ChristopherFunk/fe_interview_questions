import unittest
from q4 import validateBrackets

class BracketsValidationTests(unittest.TestCase):

    def test_valid_brackets(self):
        self.assertTrue(validateBrackets("()"))
        self.assertTrue(validateBrackets("[]"))
        self.assertTrue(validateBrackets("{}"))
        self.assertTrue(validateBrackets("<>"))
        self.assertTrue(validateBrackets("([{}])"))
        self.assertTrue(validateBrackets("{<([])>}"))
        self.assertTrue(validateBrackets("()[]{}"))
        self.assertTrue(validateBrackets("{()[]}"))

    def test_invalid_brackets(self):
        self.assertFalse(validateBrackets("["))
        self.assertFalse(validateBrackets("<"))
        self.assertFalse(validateBrackets("("))
        self.assertFalse(validateBrackets("{"))
        self.assertFalse(validateBrackets("]"))
        self.assertFalse(validateBrackets(">"))
        self.assertFalse(validateBrackets(")"))
        self.assertFalse(validateBrackets("}"))
        self.assertFalse(validateBrackets("(]"))
        self.assertFalse(validateBrackets("]("))
        self.assertFalse(validateBrackets("([)]"))
        self.assertFalse(validateBrackets("{<[])>}"))
        self.assertFalse(validateBrackets("{<([])}"))
        self.assertFalse(validateBrackets("{<([]>}"))
        self.assertFalse(validateBrackets("{<([)>}"))
        self.assertFalse(validateBrackets("{<(])>}"))

    def test_none(self):
        self.assertTrue(validateBrackets(""))
        self.assertFalse(validateBrackets(None))


if __name__ == '__main__':
    unittest.main()
