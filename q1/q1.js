function sumArray(array) {
  if (!array || !Array.isArray(array)) {
    return Error("Parameter array must be an Integer Array")
  }

  if (array.length === 0) {
    return 0
  }

  const result = array.reduce((a, b) => {
    if (a == null || isNaN(a) || b == null || isNaN(b)) {
      return Error("Parameter array must be an Integer Array")
    }

    return a + b
  })

  if (result != null) { 
    return result
  } else {
    return Error("Parameter array must be an Integer Array")
  }
}


if (process.argv[2]){
  const arg = JSON.parse(process.argv[2])

  if (arg && Array.isArray(arg)) {
    console.log('Sum: ', sumArray(arg))
  }
}


module.exports = sumArray
