const sumArray = require('./q1')

/**
 * simple expect function so we don't need to npm install a unit test lib
 */
function expect(expectedSum, sum) {
  if (expectedSum !== sum) {
    throw Error(`Sum not equal, expected: ${expectedSum}, recieved: ${sum}`)
  } else {
    console.log("Test Passed")
  }
}

/**
 * simple expect function so we don't need to npm install a unit test lib
 */
function expectError(sum) {
  if (sum instanceof Error) {
    console.log("Test Passed")
  } else {
    throw Error(`Sum not an Error, expected: Error, recieved: ${sum}`)
  }
}

// Succeeds
expect(sumArray([]), 0)
expect(sumArray([1]), 1)
expect(sumArray([8]), 8)
expect(sumArray([0, 0, 0, 0]), 0)
expect(sumArray([1, 2, 3, 4, 5, 6, 7, 8, 9]), 45)
expect(sumArray([0, 1, 2, 6, 9]), 18)
expect(sumArray([1, -2, 3, -4, 5, -6, 7, -8, 9]), 5)

// Fails
expectError(sumArray(null))
expectError(sumArray({ a: 1 }))
expectError(sumArray(['a', 'b', 'c']))
expectError(sumArray([undefined, undefined, undefined]))
expectError(sumArray([undefined, undefined, undefined]))
expectError(sumArray([null]))