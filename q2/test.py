import unittest
from q2 import checkIfStringCapitilized


class CapitalizeCheckTest(unittest.TestCase):

    def test_is_capitalized(self):
        self.assertTrue(checkIfStringCapitilized("Uppercased"))
        self.assertTrue(checkIfStringCapitilized("ALL_UPPER_CASE"))
        self.assertTrue(checkIfStringCapitilized("A"))

    def test_is_not_capitalized(self):
        self.assertFalse(checkIfStringCapitilized("lowercased"))
        self.assertFalse(checkIfStringCapitilized("notuppercased"))
        self.assertFalse(checkIfStringCapitilized("a"))
        self.assertFalse(checkIfStringCapitilized("aZ"))
        self.assertFalse(checkIfStringCapitilized(""))

    def test_none(self):
        self.assertFalse(checkIfStringCapitilized(None))


if __name__ == '__main__':
    unittest.main()
