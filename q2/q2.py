import sys

def checkIfStringCapitilized(str=""):
    if (str is None or len(str) is 0):
        return False

    firstCharOrd = ord(str[0])

    if (firstCharOrd >= 65 and firstCharOrd <= 90):
        return True

    return False


if __name__ == '__main__':
    if sys.argv[1] is None :
        print("Must pass an argument")

    print(checkIfStringCapitilized(sys.argv[1]))
