# Farmers Edge Interview Questions - Chris Funk

- python programs are written with `python2` (2.7)

- js is using node version 10 (10.17.0)

## Running Solution

- You can run q1 to q4 tests with `bash run-q1-to-q4.sh`

### Q1

#### Runnable Program

```bash
cd q2/
node q1.js [1,2,3] # JSON Array
```

#### Tests
```bash
cd q1/
node test.js
```

#### Note

- I normally would have used `jest` to run these unit tests, but currently it has no dependencies and that's kinda refreshing in the JS world.

### Q2

#### Runnable Program

```bash
cd q2/
python2 q2.py lowercase # String
python2 q2.py Uppercase # String
```

#### Tests

```bash
cd q3/
python2 test.py
```

- Runs some unit tests

### Q3

```bash
cd q3/
python2 q3.py
```

- Runs some unit tests

### Q4

#### Runnable Program

ex)

```bash
cd q4/
python2 q4.py "[[[]]]" # String
python2 q4.py "{}" # String
```

#### Tests

```bash
cd q4/
python2 test.py
```

### Q5
Weather App

Available at:

- https://my-simple-weather-app-581dc.web.app/

```bash
cd q5/weather-app/
yarn install && yarn start
```

#### Issues

- I tried using fuse.js and fuzzysearch for the search algorithm, but it was incredibly slow compared to the jumbled mess I have there. If I had more time I would look at making those fuzzy finders (likely fuse.js, looks really cool) and try to make them async, or at least not block the UI. Webworker maybe? Not 100% sure at the moment though.

- There's some magic numbers with padding and layout, I would have liked to remove those and just use:

```css
margin: auto;
max-width: 50em; // or w.e. value makes sense
```

To align everything. But alas, it didnt' work out and I ran out of time to get into that.

- The placeholder on the search box on chrome renders with a down offset :| on Firefox and Safari it works fine ¯\\\_(ツ)\_/¯

  - I suspect it has to do with the changing of `font-size` and `line-height`(?), but I played around with it for a while and couldn't get it to work properly

- The wind and temperature widgets don't wrap perfectly

- I also can't decide if I like the wind indicator or not... I have a feeling it's not the best UI/UX.

- I would also most definitely NOT have `city.list.min.json` (25MB!!!!) loaded with the solution, and would either download it on search (probably not), or just have a small micro-endpoint that does the search serverside. A lambda most likely.

  - Definitely lambda, the search is currently pretty fast and low memory: 128MB of memory to your function, executed it 30 million times in one month, and it ran for 200ms: $11.63 per month. CHEAP!

  - I hadn't found an endpoint that let me query cities by name

- Unit tests, should have done them, ran out of time. 