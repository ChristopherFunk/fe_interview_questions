import React from 'react'

import { SearchBox } from 'components/search-box'
import { WeatherView } from 'components/weather-view'
import { Container } from 'components/ui'

function App() {
  return (
    <Container>
      <SearchBox />
      <WeatherView />
    </Container>
  )
}

export default App
