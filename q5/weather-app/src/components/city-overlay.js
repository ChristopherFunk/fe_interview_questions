import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { CityHeader } from 'components/ui'
import {
  textColourDark,
  selectedColourLight,
  selectedColourDark,
  backgroundTransparentColourDark,
  backgroundTransparentColourLight,
} from 'styles/colours'

const FilterListContainer = styled.div`
  position: absolute;
  margin-top: 1em;
  padding: 0.5em 1em 0.5em 1em;
  width: 100%;
  max-width: 43em;
  min-height: 25em;
  background: ${backgroundTransparentColourLight};
  border-radius: 2.5em;
  border: 1px solid rgba(34, 36, 38, 0.15);
  z-index: 9;
  /* backdrop-filter: blur(0.55em); // Doesn't work on firefox 74 :( */

  @media (prefers-color-scheme: dark) {
    background: ${backgroundTransparentColourDark};
    color: ${textColourDark};
  }

  /* Dumb, but works for now */
  @media (max-width: 768px) {
    width: 92%;
  }

  @media (max-width: 668px) {
    width: 90%;
  }

  @media (max-width: 568px) {
    width: 88%;
  }
`

const CityContainer = styled.div`
  border-radius: 2.5em;
  padding: 0.15em 1em;
  background-color: ${(props) => (props.selected ? selectedColourLight : null)};

  @media (prefers-color-scheme: dark) {
    background-color: ${(props) =>
      props.selected ? selectedColourDark : null};
  }

  :hover {
    cursor: pointer;
    background-color: ${selectedColourLight};

    @media (prefers-color-scheme: dark) {
      background-color: ${selectedColourDark};
    }
  }
`

const EmptyList = styled.div`
  margin: auto;
  width: 100%;
`

const EmptyText = styled.h3`
  text-align: center;
  color: grey;
`

const CityOverlay = ({
  cities = [],
  selected,
  setSelected,
  selectCity,
  maxCitiesToShow,
}) => {
  const minCities = cities.slice(0, maxCitiesToShow)

  return (
    <FilterListContainer>
      {minCities.length === 0 && (
        <EmptyList>
          <EmptyText>No Cities Found</EmptyText>
        </EmptyList>
      )}
      {minCities.map((city, index) => {
        return (
          <CityContainer
            key={city.id}
            onClick={() => selectCity(city)}
            onMouseEnter={() => setSelected(index)}
            selected={selected === index}
          >
            <CityHeader
              selected={selected === index}
            >{`${city.name}, ${city.country}`}</CityHeader>
          </CityContainer>
        )
      })}
    </FilterListContainer>
  )
}

CityOverlay.propTypes = {
  cities: PropTypes.array,
  maxCitiesToShow: PropTypes.number,
  selectCity: PropTypes.func,
  selected: PropTypes.number,
  setSelected: PropTypes.func,
}

export { CityOverlay }
