import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import styled from 'styled-components'

import {
  fetchWeatherByGeoLocation,
  fetchFiveDayForcastByGeoLocation,
} from 'lib/fetch-weather'
import {
  setFetching,
  setWeatherData,
  setFiveDayForcastData,
} from 'context/context'
import {
  getHasSearchTerm,
  getWindData,
  getMainData,
  getCurrentConditons,
  getWeatherData,
  getFetching,
  getRainData,
  getSnowData,
} from 'context/selectors'
import { FlexContainer } from 'components/ui'
import { Wind, Temperature, CurrentConditions } from 'components/weather'
import { FiveDayForcast } from 'components/five-day-forcast'
import {
  backgroundColourDark,
  textColourDark,
  textColourLight,
  backgroundColourLight,
} from 'styles/colours'

const WeatherContainer = styled.div`
  margin-top: 1em;
  max-width: 100%;
  border-radius: 2.5em;
  background: ${backgroundColourLight};
  padding: 0 2em 1.5em 2em;
  border: 1px solid rgba(34, 36, 38, 0.15);
  ${(props) =>
    props.blur
      ? 'filter: blur(0.55em)'
      : null}; /* // Workaround for Blurring background on Firefox, from city-overlay.js: backdrop-filter: blur(0.55em);  */

  @media (prefers-color-scheme: dark) {
    background: ${backgroundColourDark};
    color: ${textColourDark};
  }

  @media (max-width: 768px) {
    padding: 1.5em 2em 1.5em 2em;
  }
`

const LoaderView = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: auto;
  width: 100%;
  height: 100%;
  min-height: 20em;
`

const Spinner = styled.i`
  font-size: 5em;
  color: ${textColourLight};

  @media (prefers-color-scheme: dark) {
    color: ${textColourDark};
  }

  animation: Spinner-Spin infinite 2s linear;

  @keyframes Spinner-Spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
`

const TempWindContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media (max-width: 580px) {
    flex-direction: row-reverse;
  }
`

const WeatherView = () => {
  const dispatch = useDispatch()

  // Workaround for Blurring background on Firefox, from city-overlay.js: backdrop-filter: blur(0.55em);
  const hasSearchTerm = useSelector(getHasSearchTerm)

  useEffect(() => {
    navigator.geolocation &&
      navigator.geolocation.getCurrentPosition((location) => {
        dispatch(setFetching(true))

        fetchWeatherByGeoLocation(
          location.coords.latitude,
          location.coords.longitude,
        )
          .then((response) => {
            dispatch(setWeatherData(response.data))
            dispatch(setFetching(false))
          })
          .catch((error) => {
            console.error('error', error)
            dispatch(setFetching(false))
          })

        fetchFiveDayForcastByGeoLocation(
          location.coords.latitude,
          location.coords.longitude,
        )
          .then((response) => {
            dispatch(setFiveDayForcastData(response.data))
          })
          .catch((error) => {
            console.error('error', error)
          })
      })
  }, [dispatch])

  const fetching = useSelector(getFetching)
  const weatherData = useSelector(getWeatherData)
  const wind = useSelector(getWindData)
  const main = useSelector(getMainData)
  const rain = useSelector(getRainData)
  const snow = useSelector(getSnowData)

  const currentCondition = useSelector(getCurrentConditons)

  if (fetching) {
    return (
      <WeatherContainer fetching>
        <LoaderView>
          <Spinner className="fas fa-spinner" />
        </LoaderView>
      </WeatherContainer>
    )
  }

  if (!weatherData.name) {
    return null
  }

  return (
    <WeatherContainer blur={hasSearchTerm}>
      <FlexContainer>
        <div>
          <CurrentConditions
            condition={currentCondition}
            humidity={main.humidity}
            name={`${weatherData.name}, ${weatherData.sys.country}`}
            rain={rain}
            snow={snow}
          />
        </div>

        <TempWindContainer>
          <div style={{ paddingRight: '2em' }}>
            <Wind direction={wind.deg} speed={wind.speed} />
          </div>

          <div>
            <Temperature
              feelsLike={main.feels_like}
              temperatureInK={main.temp}
            />
          </div>
        </TempWindContainer>
      </FlexContainer>

      <FiveDayForcast />
    </WeatherContainer>
  )
}

export { WeatherView }
