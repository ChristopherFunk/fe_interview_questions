import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { FlexContainer } from 'components/ui'
import { iconColour } from 'styles/colours'

const WindDirection = styled.i`
  font-size: 1.5em;
  color: black;
  transform: ${(props) =>
    props.direction ? `rotate(${props.direction - 45}deg)` : 'rotate(-45deg)'};
  color: ${iconColour};
`

const WindIcon = styled.i`
  font-size: 2.5em;
  padding-right: 0.5em;
  padding-left: 0.5em;
  color: ${iconColour};
`

const WindText = styled.p`
  text-align: center;
  font-size: 1em;
  margin: 0;
`
const getSpeedInKmH = (speed) => (speed * 3.6).toFixed(0)

const Wind = ({ direction, speed }) => {
  if (!direction || !speed) {
    return null
  }

  return (
    <>
      <FlexContainer style={{ paddingTop: '2em', paddingBottom: '1.9em' }}>
        <WindIcon className="fas fa-wind" />
        <WindDirection
          className="fas fa-location-arrow"
          direction={direction}
        />
      </FlexContainer>

      <WindText>{`${getSpeedInKmH(speed)} km/h`}</WindText>
    </>
  )
}

Wind.propTypes = {
  direction: PropTypes.number,
  speed: PropTypes.number,
}

export { Wind }
