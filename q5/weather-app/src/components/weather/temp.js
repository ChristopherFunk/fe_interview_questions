import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { FlexContainer } from 'components/ui'
import { getTempInC } from 'lib/helpers'
import { iconColour } from 'styles/colours'

const TemperatureText = styled.p`
  font-size: 2em;
`

const TemperatureGuage = styled.i`
  font-size: 2.5em;
  padding-right: 0.25em;
  color: ${iconColour};
`

const FeelLikeLabel = styled.p`
  font-size: 1em;
  margin: 0;
`

const getIcon = (tempInK) => {
  const tempInC = tempInK - 273.15

  if (tempInC < 10) {
    return 'fas fa-temperature-low'
  } else {
    return 'fas fa-temperature-high'
  }
}

const Temperature = ({ temperatureInK, feelsLike }) => {
  if (!temperatureInK) {
    return null
  }

  return (
    <>
      <FlexContainer>
        <TemperatureGuage className={getIcon(temperatureInK)} />
        <TemperatureText>
          {`${getTempInC(temperatureInK)}`}&#176;C
        </TemperatureText>
      </FlexContainer>

      {feelsLike && (
        <FeelLikeLabel>
          Feels like: {getTempInC(feelsLike)}&#176;C
        </FeelLikeLabel>
      )}
    </>
  )
}

Temperature.propTypes = {
  feelsLike: PropTypes.number,
  temperatureInK: PropTypes.number,
}

export { Temperature }
