import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { FlexContainer, ConditionIcon } from 'components/ui'
import { getCurrentConditionIcon, getCurrentConditionText } from 'lib/helpers'

const HeaderText = styled.p`
  font-size: 1.5em;
  font-weight: 600;
  margin: 0;
`

const Text = styled.p`
  margin: 0;
`

const CurrentConditions = ({ condition, name, humidity, rain, snow }) => {
  if (!condition || !name) {
    return null
  }

  return (
    <FlexContainer>
      <div>
        <ConditionIcon className={getCurrentConditionIcon(condition.icon)} />
      </div>

      <div>
        <HeaderText>{name}</HeaderText>
        <Text>{getCurrentConditionText(condition.description)}</Text>
        {rain && (
          <Text>
            <i className="fas fa-raindrops" /> &nbsp;
            {rain.toFixed(0)} mm
          </Text>
        )}

        {snow && (
          <Text>
            <i className="fas fa-snowflakes" /> &nbsp;
            {snow.toFixed(0)} mm
          </Text>
        )}

        <Text>Humidity: {humidity}%</Text>
      </div>
    </FlexContainer>
  )
}

CurrentConditions.propTypes = {
  condition: PropTypes.object,
  humidity: PropTypes.number,
  name: PropTypes.string,
  rain: PropTypes.number,
  snow: PropTypes.number,
}

export { CurrentConditions }
