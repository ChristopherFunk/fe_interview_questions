import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import styled from 'styled-components'

import { useDebounce } from 'lib/debounce'
import { fetchWeather, fetchFiveDayForcast } from 'lib/fetch-weather'
import {
  setFetching,
  setWeatherData,
  setFiveDayForcastData,
  setBlur,
} from 'context/context'
import { CityOverlay } from 'components/city-overlay'
import {
  backgroundColourDark,
  textColourDark,
  placeholderColourLight,
  placeholderColourDark,
} from 'styles/colours'

import * as cityJson from 'lib/city.list.min.json'

const citiesList = cityJson.default

const KEY_UP = 38
const KEY_DOWN = 40
const KEY_ENTER = 13
const MAX_CITIES_TO_SHOW = 5
const ESCAPE_KEY = 27

const SearchContainer = styled.div`
  margin-top: 1em;
  border-radius: 2.5em;
  padding: 0 4.25em 0 0em;
`

const Input = styled.input`
  width: 100%;
  max-width: 100%;
  border: 1px solid rgba(34, 36, 38, 0.15);
  border-radius: 5em;
  padding: 0.25em 1em;
  font-size: 32px;
  color: rgba(40, 41, 43);
  outline: none;

  ::placeholder {
    color: ${placeholderColourLight};
    font-size: 22px;
    padding: 0.25em 0;

    @media (prefers-color-scheme: dark) {
      color: ${placeholderColourDark};
    }
  }

  @media (prefers-color-scheme: dark) {
    background: ${backgroundColourDark};
    color: ${textColourDark};
  }
`

const SearchBox = () => {
  const dispatch = useDispatch()

  const [value, setValue] = useState('')
  const [cities, setCities] = useState([])
  const [selectedCity, setSelectedCity] = useState(0)

  const debouncedSearchTerm = useDebounce(value, 175)

  useEffect(() => {
    if (debouncedSearchTerm) {
      let terms = debouncedSearchTerm.replace(/,/, ' ')

      const lastIndex = terms.lastIndexOf(' ')
      terms = terms.substring(0, lastIndex) + '-fx' + terms.substring(lastIndex)
      terms = terms.split('-fx').filter((t) => t)

      const citySearch = (terms[0] || '').trim().toLowerCase()
      const countrySearch = (terms[1] || '').trim().toLowerCase()

      const matchCities = citiesList.filter((c) => {
        if (
          c.name.toLowerCase() === debouncedSearchTerm.toLowerCase() ||
          c.name.toLowerCase().includes(debouncedSearchTerm.toLowerCase())
        ) {
          return true
        }
        return countrySearch
          ? c.name.toLowerCase() === citySearch &&
              c.country.toLowerCase() === countrySearch
          : c.name.toLowerCase() === citySearch
      })

      if (matchCities.length < MAX_CITIES_TO_SHOW) {
        for (const city of citiesList) {
          if (
            !matchCities.find(
              (m) => m.name === city.name && m.country === city.country,
            )
          ) {
            if (
              countrySearch
                ? city.name.toLowerCase().includes(citySearch) &&
                  city.country.toLowerCase() === countrySearch
                : city.name.toLowerCase().includes(citySearch)
            ) {
              matchCities.push(city)
            }
          }

          if (matchCities.length >= MAX_CITIES_TO_SHOW) {
            break
          }
        }
      }

      setCities(matchCities)
    } else {
      setCities([])
    }
  }, [debouncedSearchTerm])

  const onChange = ({ target: { value } } = { target: { value: '' } }) => {
    setValue(value)
    dispatch(setBlur(!!value))
  }

  const onKeyPress = (event) => {
    switch (event.keyCode) {
      case KEY_UP: {
        if (selectedCity !== 0) {
          setSelectedCity(selectedCity - 1)
        }

        break
      }

      case KEY_DOWN: {
        if (selectedCity < MAX_CITIES_TO_SHOW - 1) {
          setSelectedCity(selectedCity + 1)
        }

        break
      }

      case KEY_ENTER: {
        if (cities.length > 0) {
          selectCity(cities[selectedCity])
          setCities([])
          onChange()
        }

        break
      }

      case ESCAPE_KEY: {
        onChange()

        break
      }

      default: {
        if (selectedCity !== 0) {
          setSelectedCity(0)
        }

        break
      }
    }
  }

  const selectCity = async (city) => {
    dispatch(setFetching(true))
    fetchWeather(city.id)
      .then((response) => {
        dispatch(setWeatherData(response.data))
        dispatch(setFetching(false))
        onChange()
      })
      .catch(() => {
        dispatch(setFetching(false))
        onChange()
      })

    fetchFiveDayForcast(city.id).then((response) => {
      dispatch(setFiveDayForcastData(response.data))
    })
  }

  return (
    <SearchContainer>
      <Input
        autoFocus
        onChange={onChange}
        onKeyDown={onKeyPress}
        placeholder="Search for a city"
        value={value}
      />
      {value ? (
        <CityOverlay
          cities={cities}
          maxCitiesToShow={MAX_CITIES_TO_SHOW}
          selectCity={selectCity}
          selected={selectedCity}
          setSelected={setSelectedCity}
        />
      ) : null}
    </SearchContainer>
  )
}

export { SearchBox }
