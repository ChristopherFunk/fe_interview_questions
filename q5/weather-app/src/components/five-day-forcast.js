import React from 'react'
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import moment from 'moment'

import { DayIcon } from 'components/ui'
import { getCurrentConditionIcon, getTempInC } from 'lib/helpers'
import { getFiveDayForcast } from 'context/selectors'

const Divider = styled.hr`
  margin-top: 1em;
  border: 1px solid rgba(151, 151, 151, 0.15);
`
const DaysContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const DayContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  text-align: center;
`

const TextContainer = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: 768px) {
    flex-direction: row;
  }
`

const Slash = styled.p`
  margin: 0.25em;
  @media (max-width: 768px) {
    margin: 0;
    display: none;
  }
`

const Text = styled.p`
  margin: 0.25em;
`

const Day = (props) => {
  return (
    <DayContainer>
      <p>{moment(props.date).format('ddd')}</p>
      <DayIcon className={getCurrentConditionIcon(props.icon)} />

      <TextContainer>
        <Text>{getTempInC(props.high)}&#176;C</Text>
        <Slash>/</Slash>
        <Text>{getTempInC(props.low)}&#176;C</Text>
      </TextContainer>
    </DayContainer>
  )
}

Day.propTypes = {
  date: PropTypes.string,
  high: PropTypes.number,
  icon: PropTypes.string,
  low: PropTypes.number,
}

const FiveDayForcast = () => {
  const data = useSelector(getFiveDayForcast)

  // The API always returns data for each 3 hours starting from 00:00:00
  const dates =
    data.list &&
    data.list.filter(
      (date) =>
        date.dt_txt.includes('12:00:00') || date.dt_txt.includes('21:00:00'),
    )

  const days = []

  if (dates && dates.length) {
    for (let i = 0; i < dates.length; i += 2) {
      const day = dates[i]
      const evening = dates[i + 1]

      days.push({
        date: day.dt_txt,
        high: day.main.temp,
        low: evening.main.temp,
        icon: day.weather[0].icon.replace('n', 'd'),
      })
    }
  }

  return (
    <div>
      <Divider />

      <DaysContainer>
        {days.map((day, index) => (
          <Day key={index} {...day} />
        ))}
      </DaysContainer>
    </div>
  )
}

export { FiveDayForcast }
