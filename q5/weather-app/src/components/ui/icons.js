import styled from 'styled-components'

import { iconColour } from 'styles/colours'

const ConditionIcon = styled.i`
  font-size: 3.5em;
  padding-right: 0.5em;
  color: ${iconColour};
`

const DayIcon = styled.i`
  font-size: 3.5em;
  color: ${iconColour};

  @media (max-width: 768px) {
    font-size: 2.5em;
  }

  @media (max-width: 650px) {
    font-size: 2em;
  }

  @media (max-width: 512px) {
    font-size: 1.5em;
  }
`

export { ConditionIcon, DayIcon }
