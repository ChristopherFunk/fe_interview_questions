import styled from 'styled-components'

import { textColourDark } from 'styles/colours'

const CityHeader = styled.h2`
  font-weight: 500;

  ${(props) => (props.selected ? `color: ${textColourDark}` : null)};

  /*
   Don't need dark mode text here, as background is always high contrast  
  */
`
export { CityHeader }
