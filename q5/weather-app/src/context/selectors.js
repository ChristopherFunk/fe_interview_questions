export const getFiveDayForcast = (state) => state.fiveDayForcast || {}
export const getHasSearchTerm = (state) => state.tempHasBlur
export const getFetching = (state) => state.fetching

export const getWeatherData = (state) => state.weatherData || {}
export const getWindData = (state) => getWeatherData(state).wind || {}
export const getMainData = (state) => getWeatherData(state).main || {}
export const getRainData = (state) => {
  const rainData = getWeatherData(state).rain

  if (rainData) {
    return rainData['3h'] || rainData['1h']
  }

  return null
}

export const getSnowData = (state) => {
  const snowData = getWeatherData(state).snow

  if (snowData) {
    return snowData['3h'] || snowData['1h']
  }

  return null
}

export const getCurrentConditons = (state) => {
  const weatherData = getWeatherData(state)
  return (weatherData.weather && weatherData.weather[0]) || {}
}
