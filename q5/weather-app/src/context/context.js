import { createStore, compose } from 'redux'

const defaultState = {
  tempHasBlur: false,
  fetching: false,
  weatherData: {},
  fiveDayForcast: {},
}

const SET_SELECTED_CITY = 'WEATHER-APP/SET_SELECTED_CITY'
const SET_FETCHING = 'WEATHER-APP/SET_FETCHING'
const SET_WEATHER_DATA = 'WEATHER-APP/SET_WEATHER_DATA'
const SET_FIVE_DAY_FORCAST_DATA = 'WEATHER-APP/SET_FIVE_DAY_FORCAST_DATA'
const SET_BLUR = 'WEATHER-APP/SET_BLUR'

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_FETCHING:
      return {
        ...state,
        fetching: action.fetching,
      }

    case SET_BLUR:
      return {
        ...state,
        tempHasBlur: action.hasBlur,
      }

    case SET_SELECTED_CITY:
      return {
        ...state,
        selectedCity: action.data,
      }

    case SET_WEATHER_DATA:
      return {
        ...state,
        weatherData: action.data,
      }

    case SET_FIVE_DAY_FORCAST_DATA:
      return {
        ...state,
        fiveDayForcast: action.data,
      }

    default:
      return state
  }
}

export const setBlur = (hasBlur) => ({
  type: SET_BLUR,
  hasBlur,
})

export const setFetching = (fetching) => ({
  type: SET_FETCHING,
  fetching,
})

export const setWeatherData = (data) => ({
  type: SET_WEATHER_DATA,
  data,
})

export const setFiveDayForcastData = (data) => ({
  type: SET_FIVE_DAY_FORCAST_DATA,
  data,
})

export const setSelectedCity = (data) => ({
  type: SET_SELECTED_CITY,
  data,
})

const enhancers = compose(
  window.__REDUX_DEVTOOLS_EXTENSION__
    ? window.__REDUX_DEVTOOLS_EXTENSION__()
    : (f) => f,
)
const store = createStore(reducer, defaultState, enhancers)
export { store }
