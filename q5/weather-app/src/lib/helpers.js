const getTempInC = (temp) => (temp - 273.15).toFixed(0)

const getCurrentConditionIcon = (conditionIcon) => {
  switch (conditionIcon) {
    case '01n':
      return 'fas fa-moon'

    case '02d':
      // few clouds
      return 'fas fa-cloud-sun'

    case '02n':
      // few clouds
      return 'fas fa-cloud-moon'

    case '03d':
      // scattered clouds
      return 'fas fa-cloud-sun'

    case '03n':
      // scattered clouds
      return 'fas fa-cloud-moon'

    case '04d':
    case '04n':
      // broken clouds
      return 'fas fa-cloud'

    case '09d':
      // shower rain
      return 'fas fa-cloud-sun-rain'

    case '09n':
      // shower rain
      return 'fas fa-cloud-moon-rain'

    case '10d':
    case '10n':
      // rain
      return 'fas fa-cloud-showers-heavy'

    case '11d':
      // thunderstorm
      return 'fas fa-thunderstorm-sun'

    case '11n':
      // thunderstorm
      return 'fas fa-thunderstorm-moon'

    case '13d':
    case '13n':
      // snow
      return 'fas fa-cloud-snow'

    case '50d':
    case '50n':
      // fog
      return 'fas fa-smog'

    case '01d':
    default:
      return 'fas fa-sun'
  }
}

const getCurrentConditionText = (conditionDescription) => {
  switch (conditionDescription) {
    case 'clear sky':
      return 'Clear sky'

    case 'few clouds':
      return 'A few clouds'

    case 'scattered clouds':
      return 'Scattered clouds'

    case 'broken clouds':
      return 'Mostly Cloudy'

    case 'shower rain':
      return 'Showers'

    case 'rain':
      return 'Rain'

    case 'thunderstorm':
      return 'Thunderstorms'

    case 'snow':
      return 'Snowing'

    case 'mist':
      return 'Mist'

    default:
      return ''
  }
}

export { getCurrentConditionIcon, getTempInC, getCurrentConditionText }
