import axios from 'axios'

const url = (cityID) =>
  `/data/2.5/weather?id=${cityID}&appid=83514100a59d4631838b399501e64e50`

const fiveDayForcastUrl = (cityID) =>
  `/data/2.5/forecast?id=${cityID}&appid=83514100a59d4631838b399501e64e50`

const geoLocationUrl = (lat, lon) =>
  `/data/2.5/weather?lat=${lat}&lon=${lon}&appid=83514100a59d4631838b399501e64e50`

const fiveDayForcastGeoLocationUrl = (lat, lon) =>
  `/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=83514100a59d4631838b399501e64e50`

const getInstance = () =>
  axios.create({
    baseURL: 'https://api.openweathermap.org',
    headers: {
      'Content-Type': 'application/json',
    },
  })

export const fetchWeather = (cityID) => {
  return getInstance().get(url(cityID))
}

export const fetchWeatherByGeoLocation = (lat, lon) => {
  return getInstance().get(geoLocationUrl(lat, lon))
}

export const fetchFiveDayForcast = (cityID) => {
  return getInstance().get(fiveDayForcastUrl(cityID))
}

export const fetchFiveDayForcastByGeoLocation = (lat, lon) => {
  return getInstance().get(fiveDayForcastGeoLocationUrl(lat, lon))
}
